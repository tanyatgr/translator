package com.example

import akka.actor._
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import spray.can.Http

import scala.concurrent.duration._

object Boot extends App {

  val config = ConfigFactory.load()
  val host = config.getString("app.host")
  val port = config.getInt("app.port")

  implicit val system = ActorSystem("translator")
  val service = system.actorOf(Props[DictionaryActor], "demo-service")

  implicit val executionContext = system.dispatcher
  implicit val timeout = Timeout(10 seconds)
  IO(Http).ask(Http.Bind(listener = service, interface = host, port = port))
    .mapTo[Http.Event]
    .map {
      case Http.Bound(address) =>
        println(s"REST interface bound to $address")
      case Http.CommandFailed(cmd) =>
        println("REST interface could not bind to " +
          s"$host:$port,${cmd.failureMessage}")
        system.shutdown()
    }

}
