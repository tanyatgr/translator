package com.example

import akka.actor._
import akka.util.Timeout
import spray.routing._
import spray.http._
import MediaTypes._
import com.typesafe.config.ConfigFactory
import scala.concurrent.duration._


class DictionaryActor extends Actor with DictionaryApi {

  def actorRefFactory = context
  def receive = runRoute(route)

}

trait DictionaryApi extends HttpService {
  
  import com.example.TranslationService

  implicit val timeout = Timeout(10 seconds)

  val route =
    pathPrefix("translate") {
      path("word") {
        //complete("OK!")
        parameters("word") { (word) =>
          requestContext =>
            val translateService = actorRefFactory.actorOf(Props(new TranslationService(requestContext)))
            translateService ! TranslationService.Process(word)
          

        }

      }

    }

}

