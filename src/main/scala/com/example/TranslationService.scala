package com.example

import akka.actor._
import akka.io.IO
import akka.event.Logging
import spray.can.client._
import spray.http._
import spray.http.HttpMethods._
import spray.http.MediaTypes._
import spray.client.pipelining._
import spray.routing.RequestContext

import scala.xml._
import scala.util.{ Success, Failure }

import java.io._

import com.typesafe.config.ConfigFactory


object TranslationService {
  case class Process(word: String)
  case class CustomRequest(key: String, text: String, lang: String)
  case class DicResult(word: String, transcription: String, translation : List[String])

}

class TranslationService(requestContext: RequestContext) extends Actor {
  import TranslationService._

  implicit val system = context.system
  import system.dispatcher

  val log = Logging(system, getClass)

  def receive = {

    case Process(word) =>
      process(word)
      log.info("inside receive method")
      context.stop(self)
  }

  def process(word: String) = {
    val config = ConfigFactory.load()
    log.info("inside process method")

    val host = config.getString("service.lookup")
    val lang = config.getString("service.lang")
    val key = config.getString("service.key")
    val filename = config.getString("service.result_file")

    val pipeline = sendReceive ~> unmarshal[NodeSeq]

    val responseFuture = pipeline {
      log.info("get send")
      Post(s"$host?key=$key&text=$word&lang=$lang")

    }
    responseFuture onComplete {
      case Success(resp) =>
        val w = (resp \\"DicResult" \ "def" \"text").text
        val ts = (resp \\ "DicResult" \ "def" \ "@ts").text
        val translations = (resp \\ "DicResult" \ "def"\ "tr" \ "text").map(_.text)

        val file = new File(filename)
        val bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file,true)))
        bw.write(s"$w  [$ts] ")
        for (t <- translations) {
         bw.write(s" -  $t \n")
        }
        bw.close()


        log.info(s"my word is : $w pronounced as $ts")
        requestContext.complete(resp)   
        
        
        
      case Failure(error) =>
        requestContext.complete(error)

    }
  }

}
